/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexao_Banco;

import Classes_Projeto.Chave;
import Classes_Projeto.Emprestimo;
import Classes_Projeto.Funcionario;
import Classes_Projeto.Local;
import Classes_Projeto.Pavilhao;
import Classes_Projeto.Reserva;
import Classes_Projeto.*;
import Classes_Projeto.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;

/**
 *
 * @author alexandra
 */
public class Manipulacao_Banco {

    private Statement statement = null; //Instanciar as classes necessárias para o Banco de Dados
    private int cont_modificacoes = 0;
    public boolean insereBanco(Usuario usuario) throws SQLException { 
        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();

        boolean inserido = false;
        this.cont_modificacoes=0;
        String comando = "INSERT INTO USUARIO VALUES('" + usuario.getCPF() + "','" + usuario.getNome() + "','" + usuario.getSenha() + "','" + usuario.getEmail() + "','" + usuario.getMatricula() + "','"+usuario.getAutorizacao()+"');";
        cont_modificacoes = statement.executeUpdate(comando);
        if(cont_modificacoes > 0){
            comando = "INSERT INTO TELEFONE VALUES ('"+usuario.getCPF()+"','"+usuario.getTelefone()+"');";
            inserido = true;
            this.cont_modificacoes=0;
            cont_modificacoes = statement.executeUpdate(comando);
            if(cont_modificacoes>0){
                inserido = true;
            }else{
                inserido = false;
            }
        }
        else{
            inserido = false;
        }
        ConexaoMysql.FecharConexao();
        return inserido;
    }

    public boolean insereBanco(Chave chave) throws SQLException {

        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        this.cont_modificacoes=0;
        boolean inserido = false;

        statement.executeUpdate("INSERT INTO CHAVE VALUES(" + chave.getIdChave() + "," + chave.getEstadoAtual() + "," + chave.getIdLocal() + ");");
        if(cont_modificacoes > 0){
            inserido = true;
        }
        else{
            inserido = false;
        }

        ConexaoMysql.FecharConexao();

        return inserido;
    }

    public boolean insereBanco(Emprestimo emprestimo, Usuario usuario, Local local) throws SQLException {

        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        this.cont_modificacoes=0;
        boolean inserido = false;
        String comando = "INSERT INTO EMPRESTIMO VALUES(0,(SELECT idUSUARIO FROM USUARIO WHERE USUARIO.matricula = '" + usuario.getMatricula() + "'), (SELECT idLOCAL FROM LOCAL WHERE LOCAL.nome ='" + local.getNome() + "'),'"+ new java.sql.Date(emprestimo.getData().getTime()) + "','" + emprestimo.getTipo() + "');";
        System.out.println(comando);
        statement.executeUpdate(comando);
        if(cont_modificacoes > 0){
                    inserido = true;
                }
                else{
                    inserido = false;
                }

        ConexaoMysql.FecharConexao();

        return inserido;
    }

    public boolean insereBanco(Local local) throws SQLException {

        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        this.cont_modificacoes=0;
        boolean inserido = false;
        String comando = "INSERT INTO LOCAL VALUES(1,'" + local.getNome() + "','" + local.getTipo() + "','" + local.getNivelAcesso() + "'," + local.getNumero() + ");";
        System.out.println(comando);
        statement.executeUpdate(comando);
        if(cont_modificacoes > 0){
            inserido = true;
        }
        else{
            inserido = false;
        }
        ConexaoMysql.FecharConexao();

        return inserido;
    }

    public boolean insereBanco(Pavilhao pavilhao) throws SQLException {

        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        this.cont_modificacoes=0;
        boolean inserido = false;

        statement.executeUpdate("INSERT INTO PAVILHAO VALUES(0,'"+ pavilhao.getNome() + "'," + pavilhao.getNumero() + ");");
        if(cont_modificacoes > 0){
            inserido = true;
        }
        else{
            inserido = false;
        }

        ConexaoMysql.FecharConexao();

        return inserido;
    }

    public boolean insereBanco(Reserva reserva, Usuario usuario, Local local) throws SQLException {

        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        this.cont_modificacoes=0;
        boolean inserido = false;
        String comando = "INSERT INTO RESERVA (`USUARIO_cpf`,`LOCAL_idLOCAL`,`data`,`tipo`) VALUES((SELECT idUSUARIO FROM USUARIO WHERE USUARIO.matricula = '" + usuario.getMatricula() + "'), (SELECT idLOCAL FROM LOCAL WHERE LOCAL.nome ='" + local.getNome() + "'),'"+ new java.sql.Date(reserva.getData().getTime()) + "','" + reserva.getTipo() + "');";
        System.out.println(comando);
        statement.executeUpdate(comando);
        if(cont_modificacoes > 0){
            inserido = true;
        }
        else{
            inserido = false;
        }

        ConexaoMysql.FecharConexao();

        return inserido;
    }

    public boolean deletaBancoUsuario(Usuario usuario) throws SQLException {

        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        this.cont_modificacoes=0;
        boolean inserido = false;
        String comando = "DELETE FROM USUARIO WHERE '" + usuario.getMatricula() + "' = USUARIO.matricula;";
        System.out.println(comando);
        statement.executeUpdate(comando);
        if(cont_modificacoes > 0){
            inserido = true;
        }
        else{
            inserido = false;
        }

        ConexaoMysql.FecharConexao();

        return inserido;
    }

    public boolean deletaBancoReserva(Reserva reserva, Local local, Usuario usuario) throws SQLException {

        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        this.cont_modificacoes=0;
        boolean inserido = false;
        String comando = "DELETE FROM RESERVA WHERE RESERVA.LOCAL_idLOCAL = (SELECT idLOCAL FROM LOCAL WHERE (LOCAL.nome = '"+local.getNome()+"') AND (LOCAL.numero = "+local.getNumero()+")) AND RESERVA.USUARIO_cpf = (SELECT idUSUARIO FROM USUARIO WHERE '" + usuario.getMatricula() + "' = USUARIO.matricula)";
        System.out.println(comando);
        statement.executeUpdate(comando);
        if(cont_modificacoes > 0){
            inserido = true;
        }
        else{
            inserido = false;
        }
        ConexaoMysql.FecharConexao();

        return inserido;
    }

    public boolean deletaBancoEmprestimo(Emprestimo emprestimo, Local local, Usuario usuario) throws SQLException {

        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        this.cont_modificacoes=0;
        boolean inserido = false;

        String comando = "DELETE FROM EMPRESTIMO WHERE EMPRESTIMO.LOCAL_idLOCAL = (SELECT idLOCAL FROM LOCAL WHERE (LOCAL.nome = '"+local.getNome()+"') AND (LOCAL.numero = "+local.getNumero()+")) AND EMPRESTIMO.USUARIO_cpf = (SELECT idUSUARIO FROM USUARIO WHERE '" + usuario.getMatricula() + "' = USUARIO.matricula)";
        System.out.println(comando);
        statement.executeUpdate(comando);
        if(cont_modificacoes > 0){
            inserido = true;
        }
        else{
            inserido = false;
        }
        ConexaoMysql.FecharConexao();

        return inserido;
    }
    
    public ResultSet recuperaEmprestimosUsuario(Usuario usuario) throws SQLException{
        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        String comando = "SELECT USUARIO.nome, EMPRESTIMO.data, LOCAL.nome, LOCAL.numero, LOCAL.nivelAcesso "
                       + "FROM EMPRESTIMO NATURAL JOIN USUARIO JOIN LOCAL ON LOCAL.idLOCAL = EMPRESTIMO.LOCAL_idLOCAL;";
        ResultSet resultSet = statement.executeQuery(comando);
        return resultSet;
    }
    
    public ResultSet recuperaEmprestimosUsuario(Usuario usuario, Date Limiteinf, Date Limitesup) throws SQLException{
        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        String comando = "SELECT USUARIO.nome, EMPRESTIMO.data, LOCAL.nome, LOCAL.numero, LOCAL.nivelAcesso "
                       + "FROM EMPRESTIMO NATURAL JOIN USUARIO JOIN LOCAL ON LOCAL.idLOCAL = EMPRESTIMO.LOCAL_idLOCAL "
                       + "WHERE EMPRESTIMO.data > "+new java.sql.Date(Limiteinf.getTime())+" AND EMPRESTIMO.data < "+new java.sql.Date(Limitesup.getTime())+";";
        ResultSet resultSet = statement.executeQuery(comando);
        return resultSet;
    }
    
    public boolean ExisteUsuario(Usuario usuario) throws SQLException{
        boolean existe = false;
        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        String comando = "SELECT * FROM USUARIO WHERE USUARIO.matricula = '"+usuario.getMatricula()+"';";
        ResultSet resultSet = statement.executeQuery(comando);
        if(resultSet.next()){
            existe = true;
        }
        return existe;
    }
    
    public boolean AtualizarUsuarioNome(Usuario usuario, String nome) throws SQLException{
        boolean atualizou = false;
        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        String comando = "UPDATE  USUARIO "
                       + "SET USUARIO.nome = '"+nome+"' "
                       + "WHERE USUARIO.matricula = '"+usuario.getMatricula()+"';";
        int modificado = statement.executeUpdate(comando);
        if(modificado>0){
            atualizou = true;
        }
        else{
            atualizou = false;
        }
        return atualizou;
    }
    
    public boolean AtualizarUsuarioSenha(Usuario usuario, String senha) throws SQLException{
        boolean atualizou = false;
        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        String comando = "UPDATE  USUARIO "
                       + "SET USUARIO.senha = '"+senha+"' "
                       + "WHERE USUARIO.matricula = '"+usuario.getMatricula()+"';";
        int modificado = statement.executeUpdate(comando);
        if(modificado>0){
            atualizou = true;
        }
        else{
            atualizou = false;
        }
        return atualizou;
    }
    
    public boolean AtualizarUsuarioEmail(Usuario usuario, String email) throws SQLException{
        boolean atualizou = false;
        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        String comando = "UPDATE  USUARIO "
                       + "SET USUARIO.email = '"+email+"' "
                       + "WHERE USUARIO.matricula = '"+usuario.getMatricula()+"';";
        int modificado = statement.executeUpdate(comando);
        if(modificado>0){
            atualizou = true;
        }
        else{
            atualizou = false;
        }
        return atualizou;
    }

    public ResultSet recuperaUsuario(Usuario usuario) throws SQLException{
        statement = ConexaoMysql.iniciaConexaoMySQL().createStatement();
        String comando = "SELECT nome, cpf, email, matricula, tipoAutorizacao, numero "
                       + "FROM USUARIO JOIN TELEFONE ON USUARIO_cpf = cpf "
                       + "WHERE USUARIO.matricula = '"+usuario.getMatricula()+"';";
        ResultSet resultSet = statement.executeQuery(comando);
        return resultSet;
    }
}