package Classes_Projeto;

import java.util.Date;

enum tipoReserva{

    AULA, PALESTRA, APRESENTACAO_TCC, OUTROS;
    
}

/**
 * Class Emprestimo
 */
public class Emprestimo {

  //
  // Fields
  //

  private Date data;
  private tipoReserva tipo;
  
  //
  // Constructors
  //
  public Emprestimo (tipoReserva tipo) {
      data = new Date();
      
      this.tipo = tipo;
  };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of data
   * @param newVar the new value of data
   */
  public void setData (Date newVar) {
    data = newVar;
  }

  /**
   * Get the value of data
   * @return the value of data
   */
  public Date getData () {
    return data;
  }

  /**
   * Set the value of tipo
   * @param newVar the new value of tipo
   */
  public void setTipo (tipoReserva newVar) {
    tipo = newVar;
  }

  /**
   * Get the value of tipo
   * @return the value of tipo
   */
  public tipoReserva getTipo () {
    return tipo;
  }

  //
  // Other methods
  //

}
