package Classes_Projeto;


import java.util.*;

enum tipoLocal{

    SALA, LABORATORIO, SALAO_PRINCIPAL, BANHEIRO;
    
}

enum tipoAutorizacao{

    SALA, SALAO_PRINCIPAL, LABORATORIO, SALAS_E_SALAO_PRINCIPAL, SALAO_E_LABORATORIO, SALAO_LABORATORIO,
    BANHEIROS, TODOS;
    
}

/**
 * Class Local
 */
public class Local {

  //
  // Fields
  //
  private String nome;
  private int numero;
  private tipoLocal tipo;
  private tipoAutorizacao nivelAcesso;
  
  //
  // Constructors
  //
  public Local (String nome, int numero, tipoLocal tipo, tipoAutorizacao nivelacesso) {
      this.nome = nome;
      this.numero = numero;
      this.tipo = tipo;
      this.nivelAcesso = nivelacesso;
  };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of nome
   * @param newVar the new value of nome
   */
  public void setNome (String newVar) {
    nome = newVar;
  }

  /**
   * Get the value of nome
   * @return the value of nome
   */
  public String getNome () {
    return nome;
  }

  /**
   * Set the value of numero
   * @param newVar the new value of numero
   */
  public void setNumero (int newVar) {
    numero = newVar;
  }

  /**
   * Get the value of numero
   * @return the value of numero
   */
  public int getNumero () {
    return numero;
  }

  /**
   * Set the value of tipo
   * @param newVar the new value of tipo
   */
  public void setTipo (tipoLocal newVar) {
    tipo = newVar;
  }

  /**
   * Get the value of tipo
   * @return the value of tipo
   */
  public tipoLocal getTipo () {
    return tipo;
  }

  /**
   * Set the value of nivelAcesso
   * @param newVar the new value of nivelAcesso
   */
  public void setNivelAcesso (tipoAutorizacao newVar) {
    nivelAcesso = newVar;
  }

  /**
   * Get the value of nivelAcesso
   * @return the value of nivelAcesso
   */
  public tipoAutorizacao getNivelAcesso () {
    return nivelAcesso;
  }

  //
  // Other methods
  //

  /**
   * @param        chave
   */
  public void adicionarChave(Chave chave)
  {
  }


  /**
   * @param        idchave
   */
  public void removerChave(int idchave)
  {
  }


}
