package Classes_Projeto;



/**
 * Class Pavilhao
 */
public class Pavilhao {

  //
  // Fields
  //

  private String nome;
  private int numero;
  
  //
  // Constructors
  //
  public Pavilhao () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of nome
   * @param newVar the new value of nome
   */
  public void setNome (String newVar) {
    nome = newVar;
  }

  /**
   * Get the value of nome
   * @return the value of nome
   */
  public String getNome () {
    return nome;
  }

  /**
   * Set the value of numero
   * @param newVar the new value of numero
   */
  public void setNumero (int newVar) {
    numero = newVar;
  }

  /**
   * Get the value of numero
   * @return the value of numero
   */
  public int getNumero () {
    return numero;
  }

  //
  // Other methods
  //

}
