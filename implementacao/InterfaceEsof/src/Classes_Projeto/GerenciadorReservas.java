package Classes_Projeto;


import java.util.*;


/**
 * Class GerenciadorReservas
 */
public class GerenciadorReservas {

  //
  // Fields
  //

  
  //
  // Constructors
  //
  public GerenciadorReservas () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  //
  // Other methods
  //

  /**
   * @param        reserva
   */
  public void adicionarReserva(Reserva reserva)
  {
  }


  /**
   * @param        numeroReserva
   */
  public void removerReserva(int numeroReserva)
  {
  }


  /**
   * @param        reserva
   */
  public void verificarDisponibilidade(Reserva reserva)
  {
  }


  /**
   * @param        nomeLocal
   * @param        numeroLocal
   */
  public void relatarUsuariosMaisFrenguentesPorLocal(String nomeLocal, int numeroLocal)
  {
  }


  /**
   * @return       unsigned int
   * @param        chaves_
   */
  public int relatarQuantidadeChavesNaoDevolvidas(Chave chaves)
  {
      return 0;
  }


  /**
   * @param        locais_
   */
  public void relatarEspacosMaisUtilizados(Local locais_)
  {
  }


  /**
   * @return       unsigned int
   * @param        chaves_
   */
  public void relatarChavesDesaparecidas(Chave chaves)
  {
  }


}
