SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `key_control` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `key_control` ;

-- -----------------------------------------------------
-- Table `key_control`.`LOCAL`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `key_control`.`LOCAL` (
  `idLOCAL` INT UNSIGNED NOT NULL ,
  `nome` VARCHAR(300) NOT NULL ,
  `tipo` ENUM('SALA', 'LABORATORIO', 'SALAO PRINCIPAL', 'BANHEIRO') NOT NULL ,
  `nivelAcesso` ENUM('SALA', 'SALAO PRINCIPAL', 'LABORATORIOS', 'SALAS E SALAO PRINCIPAL', 'SALAS E LABORATORIOS', 'SALAO E LABORATORIOS','BANHEIROS','TODOS') NOT NULL ,
  `numero` INT NOT NULL ,
  PRIMARY KEY (`idLOCAL`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `key_control`.`CHAVE`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `key_control`.`CHAVE` (
  `idChave` INT UNSIGNED NOT NULL ,
  `estadoAtual` ENUM('EM_USO', 'DISPONIVEL', 'NAO DEVOLVIDA', 'EXTRAVIADA') NOT NULL ,
  `LOCAL_idLOCAL` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`idChave`) ,
  INDEX `fk_CHAVE_LOCAL1` (`LOCAL_idLOCAL` ASC) ,
  CONSTRAINT `fk_CHAVE_LOCAL1`
    FOREIGN KEY (`LOCAL_idLOCAL` )
    REFERENCES `key_control`.`LOCAL` (`idLOCAL` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `key_control`.`PAVILHAO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `key_control`.`PAVILHAO` (
  `idPAVILHAO` INT NOT NULL ,
  `nome` VARCHAR(45) NOT NULL ,
  `numero` INT NOT NULL ,
  `LOCAL_idLOCAL` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`idPAVILHAO`) ,
  INDEX `fk_PAVILHAO_LOCAL1` (`LOCAL_idLOCAL` ASC) ,
  CONSTRAINT `fk_PAVILHAO_LOCAL1`
    FOREIGN KEY (`LOCAL_idLOCAL` )
    REFERENCES `key_control`.`LOCAL` (`idLOCAL` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `key_control`.`USUARIO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `key_control`.`USUARIO` (
  `idUSUARIO` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nome` VARCHAR(100) NOT NULL ,
  `senha` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(45) NOT NULL ,
  `matricula` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idUSUARIO`) ,
  UNIQUE INDEX `UNIQUE` (`nome` ASC, `senha` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `key_control`.`RESERVA`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `key_control`.`RESERVA` (
  `LOCAL_idLOCAL` INT UNSIGNED NOT NULL ,
  `USUARIO_cpf` INT UNSIGNED NOT NULL ,
  `data` DATE NOT NULL ,
  `tipo` ENUM('AULA', 'PALESTRA', 'APRESENTACAO TCC', 'OUTRAS') NOT NULL ,
  PRIMARY KEY (`LOCAL_idLOCAL`, `USUARIO_cpf`) ,
  INDEX `fk_RESERVA_USUARIO1` (`USUARIO_cpf` ASC) ,
  CONSTRAINT `fk_RESERVA_LOCAL1`
    FOREIGN KEY (`LOCAL_idLOCAL` )
    REFERENCES `key_control`.`LOCAL` (`idLOCAL` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RESERVA_USUARIO1`
    FOREIGN KEY (`USUARIO_cpf` )
    REFERENCES `key_control`.`USUARIO` (`idUSUARIO` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `key_control`.`EMPRESTIMO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `key_control`.`EMPRESTIMO` (
  `idEMPRESTIMO` INT NOT NULL ,
  `USUARIO_cpf` INT UNSIGNED NOT NULL ,
  `LOCAL_idLOCAL` INT UNSIGNED NOT NULL ,
  `data` DATE NOT NULL ,
  `tipo` ENUM('AULA', 'PALESTRA', 'APRESENTACAO TCC', 'OUTRAS') NOT NULL ,
  PRIMARY KEY (`idEMPRESTIMO`) ,
  INDEX `fk_EMPRESTIMO_USUARIO1` (`USUARIO_cpf` ASC) ,
  INDEX `fk_EMPRESTIMO_LOCAL1` (`LOCAL_idLOCAL` ASC) ,
  CONSTRAINT `fk_EMPRESTIMO_USUARIO1`
    FOREIGN KEY (`USUARIO_cpf` )
    REFERENCES `key_control`.`USUARIO` (`idUSUARIO` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_EMPRESTIMO_LOCAL1`
    FOREIGN KEY (`LOCAL_idLOCAL` )
    REFERENCES `key_control`.`LOCAL` (`idLOCAL` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `key_control`.`TELEFONE`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `key_control`.`TELEFONE` (
  `USUARIO_cpf` INT UNSIGNED NOT NULL ,
  `numero` INT NOT NULL ,
  PRIMARY KEY (`USUARIO_cpf`) ,
  CONSTRAINT `fk_TELEFONE_USUARIO1`
    FOREIGN KEY (`USUARIO_cpf` )
    REFERENCES `key_control`.`USUARIO` (`idUSUARIO` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
