package Classes_Projeto;

enum estado{
    
    EM_USO, DEVOLVIDA, NAO_DEVOLVIDA, EXTRAVIADA; 
    
}

/**
 * Class Chave
 */
public class Chave {

  //
  // Fields
  //
  private estado estadoAtual;
  private int idChave;
  private int idLocal;
  
  //
  // Constructors
  //
  public Chave () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of estadoAtual
   * @param newVar the new value of estadoAtual
   */
  public void setEstadoAtual (estado newVar) {
    estadoAtual = newVar;
  }

  /**
   * Get the value of estadoAtual
   * @return the value of estadoAtual
   */
  public estado getEstadoAtual () {
    return estadoAtual;
  }

  /**
   * Set the value of idChave
   * @param newVar the new value of idChave
   */
  public void setIdChave (int newVar) {
    idChave = newVar;
  }

  /**
   * Get the value of idChave
   * @return the value of idChave
   */
  public int getIdChave () {
    return idChave;
  }

  public void setIdLocal(int newVar){
      idLocal = newVar;
  }
  
  public int getIdLocal(){
      return idLocal;
  }
  //
  // Other methods
  //

}
