package Classes_Projeto;


import java.util.*;


/**
 * Class Reserva
 */
public class Reserva {

  //
  // Fields
  //

  private Date data;
  private tipoReserva tipo;
  private int numero;
  
  //
  // Constructors
  //
  public Reserva (tipoReserva tipo) { 
      this.data = new Date();
      this.tipo = tipo;
  };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of data
   * @param newVar the new value of data
   */
  public void setData (Date newVar) {
    data = newVar;
  }

  /**
   * Get the value of data
   * @return the value of data
   */
  public Date getData () {
    return data;
  }

  /**
   * Set the value of tipo
   * @param newVar the new value of tipo
   */
  public void setTipo (tipoReserva newVar) {
    tipo = newVar;
  }

  /**
   * Get the value of tipo
   * @return the value of tipo
   */
  public tipoReserva getTipo () {
    return tipo;
  }

  /**
   * Set the value of numero
   * @param newVar the new value of numero
   */
  private void setNumero (int newVar) {
    numero = newVar;
  }

  /**
   * Get the value of numero
   * @return the value of numero
   */
  private int getNumero () {
    return numero;
  }

  //
  // Other methods
  //

}
