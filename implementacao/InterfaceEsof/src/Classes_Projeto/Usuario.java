package Classes_Projeto;


import java.util.*;


/**
 * Class Usuario
 */
public class Usuario {

  //
  // Fields
  //
  private String CPF;
  private String nome;
  private String email;
  private int telefone;
  private String matricula;
  private String senha;
  private tipoAutorizacao autorizacao;
  
    //
    // Constructors
    //
    public Usuario(String CPF, String nome, String email, int telefone, String matricula, String senha, tipoAutorizacao autorizacao) {
        this.CPF = CPF;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.matricula = matricula;
        this.senha = senha;
        this.autorizacao = autorizacao;
    }

    public Usuario(String text, String text0, String text1, String text2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
    //
    // Methods
    //
    //
    // Accessor methods
    //
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getCPF() {
        return CPF;
    }

    public void setCPF(String cpf) {
        this.CPF = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTelefone() {
        return telefone;
    }

    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public void setAutorizacao(tipoAutorizacao x){
        this.autorizacao = x;
    }
    
    public tipoAutorizacao getAutorizacao(){
        return this.autorizacao;
    }
    

  //
  // Other methods
  //

  /**
   * @param        usuario
   */
  public void editarEndereco(Usuario usuario)
  {
  }


  /**
   * @param        numero
   */
  public void adicionarTelefone(String numero)
  {
  }


  /**
   * @param        index
   */
  public void removerTelefone(int index)
  {
  }


}
