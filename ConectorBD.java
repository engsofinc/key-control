//Código exemplo para o Minicurso de Banco de Dados da SATEL
//Profa. Gláucia Braga e Silva
//Classe que implementa a conexão com a base de dados do MySQL
package acessodb;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConectorBD {    
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
   static final String DATABASE_URL = "jdbc:mysql://localhost/key_control"; 
   private Connection connection = null;
   private Statement statement = null;
    
   //estabelece uma conexão com a base de dados via driver JDBC
   public Connection connectBD(){  
        try
        {            
            Class.forName(JDBC_DRIVER);            
            connection = DriverManager.getConnection(DATABASE_URL, "root", "cabral1888");//alterar o segundo root para fepam                 
            return connection;
           
        }
        catch(SQLException sqlException){
            System.out.println(connection.toString());
            return connection;
        }
        catch(ClassNotFoundException classNotFound){
            return connection;
        }       
   }
   
   public void closeConnection(){
       try{
                connection.close();
            }
            catch(Exception exception){
                System.exit(1);
            }
   }
   
   public int IsertDB(Object x){
       try {
           statement = this.connectBD().createStatement();
       } catch (SQLException ex) {
           Logger.getLogger(ConectorBD.class.getName()).log(Level.SEVERE, null, ex);
       }
       int result=0;
       
       if(x instanceof Local){
           
       }
       if(x instanceof Chave){
           statement.executeUpdate("INSERT INTO CHAVE VALUES("+Chave.getIdChave()+","+0+","+Chave.getEstadoAtual()+","+Chave.getSala()+");" );
       }
       if(x instanceof Pavilhao){
           
       }
       if(x instanceof Reserva){
           
       }
       if(x instanceof Usuario){
           
       }
       if(x instanceof Chave){
           
       }
       if(x instanceof Funcionario){
           
       }
       if(x instanceof Emprestimo){
           
       }
       return result;
   }
    
}

