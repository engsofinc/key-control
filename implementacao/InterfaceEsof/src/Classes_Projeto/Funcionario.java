package Classes_Projeto;



/**
 * Class Funcionario
 */
public class Funcionario {

  //
  // Fields
  //

  private String nome;
  private int id;
  private String senha;
  
  //
  // Constructors
  //
  public Funcionario () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of nome
   * @param newVar the new value of nome
   */
  private void setNome (String newVar) {
    nome = newVar;
  }

  /**
   * Get the value of nome
   * @return the value of nome
   */
  private String getNome () {
    return nome;
  }

  /**
   * Set the value of id
   * @param newVar the new value of id
   */
  private void setId (int newVar) {
    id = newVar;
  }

  /**
   * Get the value of id
   * @return the value of id
   */
  private int getId () {
    return id;
  }

  /**
   * Set the value of senha
   * @param newVar the new value of senha
   */
  private void setSenha (String newVar) {
    senha = newVar;
  }

  /**
   * Get the value of senha
   * @return the value of senha
   */
  private String getSenha () {
    return senha;
  }

  //
  // Other methods
  //

}
